<?php

namespace BLL;

use DAL;
require_once 'BaseClass.php';
require_once DIR_DAL . 'user.php';

class User extends BaseClass {

    public $db;
    public $userID;
    public $customerID;
    public $firstName;
    public $lastName;
    public $email;
    public $password;
    public $mobile;
    public $imei;
    public $lastModified;

    public function __construct($db, $userID = null) {
        $this->db = $db;
        if ($userID != NULL) {
            $user = new DAL\User($this->db);
            $data = $user->getUser($userID);
            $details = $data[0];
            $this->userID = $details['user_id'];
            $this->customerID = $details['customer_id'];
            $this->firstName = $details['first_name'];
            $this->lastName = $details['last_name'];
            $this->password = $details['password'];
            $this->email = $details['email'];
            $this->mobile = $details['mobile'];
            $this->imei = $details['imei'];
            $this->lastModified = $details['last_modified'];

        } else {
            $this->userID = NULL;
            $this->customerID = NULL;
            $this->firstName = NULL;
            $this->lastName = NULL;
            $this->email = NULL;
            $this->mobile = NULL;
            $this->password = NULL;
            $this->imei = NULL;
            $this->lastModified = NULL;
        }
    }
    
    public function getUserByMobile() {
        $user = new DAL\User($this->db);
        return $user->getUserByMobile($this->mobile);
    }
    
    /* function to fetch data from user registration by mobile
     * 
     */
    public function getUserFromUserRegistration() {
        $user = new DAL\User($this->db);
        return $user->getUserFromUserRegistration($this->mobile);
    }
    public function getUserByCustomerID() {
        $user = new DAL\User($this->db);
        return $user->getUserByCustomerID($this->customerID);
    }

    public function updateIMEI() {
        $user = new DAL\User($this->db);
        return $user->updateIMEI($this->userID, $this->imei);
    }

    public function updatePassword() {
        $user = new DAL\User($this->db);
        $userDetails = $user->getUserByMobile($this->mobile);
        if(count($userDetails)>0)
        {
            return $user->updatePassword($userDetails[0]['user_id'], $this->password);
        }
        else
        {
            return -1;
        }

    }

    public function addUser($duplicateError=false) {
        $user = new DAL\User($this->db);
        $existingUser = $user->getUserByMobile($this->mobile);
        if(count($existingUser)>0)
        {
            return false;
        }
        return $user->addUser($this->firstName,$this->lastName,$this->email,$this->password,$this->mobile,$this->imei,$this->customerID, $duplicateError);
    }
    
    public function updateUser() {
        $user = new DAL\User($this->db);
        return $user->updateUser($this->firstName,$this->lastName,$this->email,$this->password,$this->mobile,$this->imei);
    }
    
     public function deleteUser() {
        $user = new DAL\User($this->db);
        return $user->deleteUser($this->userID);
    }
    
    public function addUserRegistration() {
        $user = new DAL\User($this->db);
        $user->removeUserRegistration($this->mobile);
        return $user->addUserRegistration($this->firstName,$this->lastName,$this->email,$this->password,$this->mobile,$this->imei);
    }
    

    /* Code for User Details Update
     *
     */
    
    public function addUserUpdate(){
    	$user = new DAL\User($this->db);
    	$user->removeUserUpdate($this->userID);
    	return $user->addUserUpdate($this->userID,$this->firstName,$this->lastName,$this->email,$this->password,$this->mobile,$this->imei);
    }
    
    public function getUserFromUserUpdate() {
    	$user = new DAL\User($this->db);
    	return $user->getUserFromUserUpdate($this->userID);
    }
    
    public function updateUserByID() {
    	$user = new DAL\User($this->db);
    	return $user->updateUserByID($this->userID,$this->firstName,$this->lastName,$this->email,$this->password,$this->mobile,$this->imei);
    }
}
