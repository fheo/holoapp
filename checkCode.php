

<?php

error_reporting(E_ERROR);
//require_once 'common.php';
require_once 'constants.php';
require_once 'masterConnection.php';
require_once 'functions.php';
include_once 'globals.php';
require 'vendor/autoload.php';
require_once DIR_BLL . 'codes.php';
require_once DIR_BLL . 'codeLog.php';

require_once 'Logger.php';
$logger = new Logger();


$content = trim(isset($_POST["code"])?$_POST["code"]:'');
$ipAddr1 = isset($_POST["ipAddr1"])?$_POST["ipAddr1"]:'';
$ipAddr2 = isset($_POST["ipAddr2"])?$_POST["ipAddr2"]:'';
$url = isset($_POST["url"])?$_POST["url"]:'';



//spiltting to seperate keyword from code
$data   = preg_split('/\s+/', $content);
if(count($data)> 1){
	$uid = $data[1];
	$keyword = $data[0];
}else{
	$uid = $data[0]	;
}


// Check if variables are not empty
$inputs = array("uid"=>$uid);
$result =checkInputs($inputs);
if ($result["error"]) {
	echo 'F|'.$result["msg"].' field(s) are required.';
	die();
}

$dataReceived = array(
		globals::$P_CONTENT => $content,
);

//insert check in Log

$codeLogObject = new BLL\codeLog($db);
$codeLogObject->UID = $uid;
$codeLogObject->ipAddr1 = $ipAddr1;
$codeLogObject->ipAddr2 = $ipAddr2;
$codeLogObject->url = $url;
$codeLogObject->addCodeLog();


$codeObject = new BLL\codes($db);

$codeObject->UID = $uid;
$result = $codeObject->getCodeByUID();
if(!empty($result)){
	if($result["is_active"] ==1){
		if($result["is_valid"] == 1){
			//$message =  $uid." is a valid code. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
			$message =  "This is a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
			$codeObject->setCodeInvalid();
		}
		elseif($result["isValid"] == 0){
			//$message =  $uid." was a valid code but isn't valid anymore . Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
			$message =  "This is not a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
		}
		
	}else{
		//$message =  $uid." code is valid but hasn't been activated yet. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
		$message =  "This is not a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
	}
	
}else{
	//$message =  $uid." is an invalid code. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
	$message =  "This is not a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
}

echo $message;	
unset($codeObject);
unset($result);
	


