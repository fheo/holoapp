<?php
session_start();
if (isset($_POST['username']) && isset($_POST['password'])) {
    require_once '../constants.php';
    require_once DIR_BLL . 'adminUser.php';
    require_once DIR_BLL . 'store.php';
    require_once DIR_BLL . 'storeChain.php';
    require_once DIR_BLL . 'permission.php';
    require_once '../masterConnection.php';
    $adminUser = new BLL\AdminUser($db);
    $adminUser->username = $_POST['username'];
    $adminUser->password = $_POST['password'];
    $userDetail = $adminUser->getUserByCredentials();
   
   
    
    if(count($userDetail)>0 && isset($userDetail[0]['username']))
    {
        $_SESSION['userID']=$userDetail[0]['id'];
        if($_SESSION['userID']==1 || $_SESSION['userID']==54)
        {
            $_SESSION['isSuperAdmin']=true;
        }
        else
        {
            $_SESSION['isSuperAdmin']=false;
        }
        $_SESSION['storeID']=$userDetail[0]['store_id'];        
        $_SESSION['storeChainID']=$userDetail[0]['store_chain_id'];
        $_SESSION['firstName']=$userDetail[0]['first_name'];
        $_SESSION['lastName']=$userDetail[0]['last_name'];
        $_SESSION['isAdmin']=$userDetail[0]['is_admin'];
        $_SESSION['roleID']=$userDetail[0]['role_id'];

        
        /* This session is stored for admin which is used in stats and analytics */
        $getAllStores = new BLL\Store($db);
        
        $arrStoreChain="";
        if($_SESSION['roleID']==1 || $_SESSION['roleID']==2){
          $arrAllStores = $getAllStores->getAllStores();  
            $storeChain = new BLL\StoreChain($db);
            $arrStoreChain = $storeChain->getAllStoreChain();
           
        }elseif($_SESSION['roleID']==3){
        
         $getAllStores->storeChainID=$userDetail[0]['store_chain_id'];
         $arrAllStores = $getAllStores->getStoreByChain();
        }elseif($_SESSION['roleID']==4){
        
         $getAllStores->storeID=$userDetail[0]['store_id'];
         $arrAllStores = $getAllStores->getStoresById();
        }
        
        $_SESSION['storesToShow']=$arrAllStores;
        $_SESSION['storeChainsToShow']=$arrStoreChain;
        
//        foreach($arrAllStores as $allStores){
//        $stores[]=$allStores['id'];
//        }
//        $_SESSION['storesSelected']=$stores;

/* Get Access permission by role id*/
        $permission = new BLL\Permission($db,$_SESSION['roleID']);
        $_SESSION['usercreatepermissions'] = $permission->getAllPermissionsByUserRole();
/* Get Access permission by role id*/        
         
        if($_SESSION['isAdmin']==1)
        {
            
            $store = new BLL\Store($db);
            $store->storeChainID = $_SESSION['storeChainID'];
            $_SESSION['storesAccessible'] = $store->getStoreByChain();
        }
        else
        {
            $_SESSION['storesAccessible'] = array($_SESSION['storeID']);
        }
        header("Location: dashboard.php");
        die();
    }
     header("Location: loginFails.php");
        die();
} else {
    header("Location: index.php?error=1");
    die();
}

