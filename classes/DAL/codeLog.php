<?php

namespace DAL;

use DAL;
use PDO;
require_once DIR_DAL . 'BaseClass.php';
require_once DIR_CLASSES . 'encryption/AES.php';

class codeLog extends BaseClass {

	public function addCodeLog($UID,$ipAddr1, $ipAddr2, $mobile, $userId,$url) {
        $sql = "INSERT INTO code_log(uid,ip_addr1,ip_addr2,mobile,user_id,url,created_on) VALUES(:UID,:ipAddr1,:ipAddr2,:mobile,:userId,:url,now())";

        $stmt = $this->db->prepare($sql);
        
        $stmt->bindParam(':UID', $UID);
        $stmt->bindParam(':ipAddr1', $ipAddr1);
        $stmt->bindParam(':ipAddr2', $ipAddr2);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':userId', $userId);
        $stmt->bindParam(':url', $url);
        $result = $stmt->execute();
        return $result;
	
	}
	
	
	
	    
}
