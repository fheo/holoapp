<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<LINK HREF="css/page-style.css" REL="stylesheet" TYPE="text/css">

  <meta charset="utf-8">
  <title>Holosecurity Authenticity Verification System</title>
  <script src="js/jquery-1.js"></script>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<body>
<div class="logoheader">

<img src="img/logo.jpg">

 </div>
 <ul class="topnav" id="myTopnav">
  <li><a class="active" href="http://holoscratchapp.azurewebsites.net/cc.php?id">Home</a></li>
  <li><a href="http://holosecurity.com/about-us.html">About US</a></li>
  <li><a href="http://holosecurity.com/holograms-stickers.html">Our Product</a></li>
  <li><a href="http://holosecurity.com/enquiry.html">Contact Us</a></li>
  <li class="icon">
    <a href="javascript:void(0);" style="font-size:15px;" onclick="myFunction()">☰</a>
  </li>
</ul>
<div class="verifysection">


<div class="verifyform">	
<p class="Text-1"><h1>! Validity Verification !</h1></p>
<br>


<img style="margin-left:10px;" src="img/label-1.png">
<form action="checkCode.php" id="searchForm">
  <input class="inputbg"  title="Enter Label Serial Number" placeholder="Enter Label Serial No" name="code" type="text" value="<?php echo ((isset($_GET["id"]))?htmlspecialchars($_GET["id"]):""); ?>">
  <input class="btnbg" value="Verify" type="submit">
</form>
<br>
<br>


<!-- the result of the search will be rendered inside this div -->
<div id="result" class="resultText"></div>
</div>

<div class="context1"><p><h1>How&nbspTo&nbspFind&nbspGenuine Product ?</h1>For All Products there is a unique design of authentication devices that deliver the most secure, effective, efficient, and valuable benefits. The accurate and reliable supply of these individually identified authentication devices from production in our security facilities to application in our Client’s factories, is the basis for controlling and monitoring the production of each Brand’s authorized products globally.</p>

<p><h1>About Us</h1>Authentication Technologies are essential to all genuine products and packaging. <b>Holosecurity Technologies</b> has been delivering the leading authentication technologies to top brands for over 15 years.</p>
<p>
<h1>Inspect Visually</h1>
<img id="myImage" src="img/preview1.gif" ><br>
<button onclick="document.getElementById('myImage').src='img/e-beam1.gif'">Turn On The Animation</button><br>
<button onclick="document.getElementById('myImage').src='img/preview1.gif'">Turn Off The Animation</button><br>
<br>
<b>If Your Seal Matches The Below Description It is A Genuine Product</b><br><br></p>
<p style="color: #016d21"><b>1.</b>&nbsp &nbsp When the label is tilted left to right, you will see the logo colors change.<br></p>
<p style="color: #016d21"><b>2.</b>&nbsp &nbsp When the label is tilted 90 degrees top to bottom the word ORIGINAL will be visible.<br></p>
<p style="color: #016d21"><b>3.</b>&nbsp &nbsp When the label is tilted left to right, you will see that the background animate.<br></p>

</div>
</div>

 
<script>
// Attach a submit handler to the form
$( "#searchForm" ).submit(function( event ) {
 
  // Stop form from submitting normally
  event.preventDefault();
 
  // Get some values from elements on the page:
  var $form = $( this ),
    codeValue = $form.find( "input[name='code']" ).val(),
    url = $form.attr( "action" );
 
  // Send the data using post
  var posting = $.post( url, { code: codeValue } );
 
  // Put the results in a div
  posting.done(function( data ) {
 
  	 
    $( "#result" ).empty().append( data );
  });
});
</script>

<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
</script>
 



	


</body></html>