<?php
error_reporting(E_ALL);
//require_once 'common.php';
require_once 'constants.php';
require_once 'masterConnection.php';
require_once 'functions.php';
include_once 'globals.php';
require_once DIR_BLL . 'Codes.php';

require_once 'Logger.php';
$logger = new Logger();

$numRolls = isset($_POST[globals::$P_NUM_ROLLS])?$_POST[globals::$P_NUM_ROLLS]:'';
$numLabelsPerRoll = isset($_POST[globals::$P_NUM_LABELS_PER_ROLL])?$_POST[globals::$P_NUM_LABELS_PER_ROLL]:'';
$customerName =  isset($_POST[globals::$P_CUSTOMER_NAME])?$_POST[globals::$P_CUSTOMER_NAME]:'';

$inputs = array("numRolls"=>$numRolls,"numLabelsPerRoll"=>$numLabelsPerRoll,"customerName"=>$customerName);
$result =checkInputs($inputs);

if ($result["error"]) {

	$logger->logEvent('generateCodes', $dataReceived, array('ERROR'=>'F|'.$result["msg"].' field(s) are required.'), 'ERROR_INVALID_INPUT');
	echo 'F|'.$result["msg"].' field(s) are required.';
	die();
}

$totalLabels = $numRolls*$numLabelsPerRoll;

$codeObject = new BLL\Codes($db);
$rollNumber = $codeObject->getLastRollNumber() + 1;
$oid = $codeObject->getLastOID();
$existingRandom = $random = array_flip($codeObject->getAllCodes());

//$time_start = microtime(true);
$existingCount = count($random); //The codes you already have

do {
	$random[mt_rand(100000000000,999999999999)] = 1;
} while ((count($random)-$existingCount) < $totalLabels);
$newCodes = array_diff_key($random,$existingRandom);
$newCodes = array_keys($newCodes);

echo 'done2';
$aValues = array();
$i=0;
$isActive = 0;
$isValid = 1;
$newCodesArray[] = array('uid','oid',RollNumber,CustomerName);
foreach($newCodes as $row){
	$oid++;
	if($i == $numLabelsPerRoll){$rollNumber++; $i =0;}
	array_push($aValues,$oid, $row, $rollNumber,$customerName,$isActive,$isValid,date('Y-m-d H:i:s'));
	$newCodesArray[] = array($row,$oid,$rollNumber,$customerName);
	$i++;
}

unset($existingRandom);
unset($random);
$codeObject->addCode($aValues,$totalLabels);
$fileName = $customerName."_scratchcodes_" . date('Ymdhs') . ".csv";
echo 'done3';
//convert_to_csv($newCodesArray,$fileName,',');
unset($newCodes);
unset($newCodesArray);
unset($codeObject);
unset($aValues);

 




function convert_to_csv($input_array, $fileName, $delimiter)
{
	
    header('Content-Type: text/csv');
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	/* open raw memory as file, no need for temp files, be careful not to run out of memory thought */
	$f = fopen('php://output', 'w');
	/* loop through array  */
	foreach ($input_array as $line) {
		/* default php csv handler */
		
		fputcsv($f, $line, $delimiter);
	}
	fclose($f) or die("Can't close php://output");

}





