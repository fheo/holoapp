<?php

/**
 * Menu: Alerts
 *
 * create by : RK:start Alerts
 * QSKIP
 */

namespace DAL;

use DAL;
require_once DIR_DAL.'BaseClass.php';

class Alert extends BaseClass {

    public function getAlert($startDate,$endDate,$stores) {
      
//        
//        if($_SESSION['roleID']==1 || $_SESSION['roleID']==2){
//        $sql = "SELECT * FROM web_alerts wa LEFT JOIN web_alert_messages am ON wa.message_id=am.id LEFT JOIN store s ON wa.store_id=s.id LEFT JOIN store_chain sc ON sc.id=wa.storechain_id WHERE date(wa.createdOn)='".$today_date."'";
//        }elseif($_SESSION['roleID']==3){
//        $sql = "SELECT * FROM web_alerts wa LEFT JOIN web_alert_messages am ON wa.message_id=am.id LEFT JOIN store s ON wa.store_id=s.id LEFT JOIN store_chain sc ON sc.id=wa.storechain_id WHERE date(wa.createdOn)='".$today_date."' AND sc.id=$storeChainID";    
//        }else{
//        $sql = "SELECT * FROM web_alerts wa LEFT JOIN web_alert_messages am ON wa.message_id=am.id LEFT JOIN store s ON wa.store_id=s.id  WHERE date(wa.createdOn)='".$today_date."' AND s.id=$storeID";    
//        }
        
        if(is_array($stores)){
        $stores = implode($stores, ',');
        }
        else $stores = $stores;
//        print_r($stores);
        $sql = "SELECT * FROM web_alerts wa LEFT JOIN web_alert_messages am ON wa.message_id=am.id LEFT JOIN store s ON s.id=wa.store_id LEFT JOIN store_chain sc ON sc.id=s.store_chain_id WHERE  date(wa.createdOn) between :startDate and :endDate AND wa.store_id in ($stores)";
           
        
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        $result = $stmt->fetchAll();
        
        return $result;
    }
    
    public function getAlertsById($alertID){
       
        $sql = "SELECT *,s.name as storename FROM web_alerts wa LEFT JOIN web_alert_messages am ON wa.message_id=am.id LEFT JOIN admin_user au ON au.id=wa.user_id LEFT JOIN store s ON s.id=wa.store_id LEFT JOIN store_chain sc ON sc.id=wa.storechain_id  LEFT JOIN product p ON p.id=wa.product_id WHERE wa.id=:alertID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':alertID', $alertID);
        $stmt->execute();
        $result = $stmt->fetchAll();
     
        return $result;
    }
    

    public function createAlert($storeID, $storeChainID, $messageID, $userID,$productID, $productCode, $cartID,$orderID,$status) {
    	
    	try{
    	$sql = "INSERT INTO web_alerts(store_id, storechain_id,message_id,user_id,product_id,product_code,cart_id,order_id,createdOn,status) VALUES (:storeID, :storeChainID, :messageID, :userID,:productID,:productCode, :cartID,:orderID,now(),:status)";
    	$stmt = $this->db->prepare($sql);
    	
    	$stmt->bindParam(':storeID', $storeID);
    	$stmt->bindParam(':storeChainID', $storeChainID);
    	$stmt->bindParam(':messageID', $messageID);
    	$stmt->bindParam(':userID',$userID);
    	$stmt->bindParam(':productID', $productID);
    	$stmt->bindParam(':productCode', $productCode);
    	$stmt->bindParam(':cartID',$cartID);
    	$stmt->bindParam(':orderID',$orderID);
    	$stmt->bindParam(':status',$status);
    	$result = $stmt->execute();
    	if ($result == false)
    		return 0;
    		else
    			return($this->db->lastInsertId());
    	}
    	catch(\PDOException $E){
    		//"product already in db for that store with 0 hence not created any entry in db"
    		return 0;
    	}
    }
    
    
    
    public function updateAlertStatus($alertID,$status){
        $sql="UPDATE web_alerts SET status=:status WHERE id=:alertID";
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam(':alertID', $alertID);
         $stmt->bindParam(':status', $status);
         $result = $stmt->execute();
         return $result;
    }
    

    public function addHistory($userID, $alertID, $comments, $status){
        
        $sql = "INSERT INTO web_alerts_history(user_id,alertid,comments,status,createdOn) "
                . "VALUES(:user_id,:alert_id,:comments,:status,now());";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':user_id', $userID);
        $stmt->bindParam(':alert_id', $alertID);
        $stmt->bindParam(':comments', $comments);    
        $stmt->bindParam(':status', $status);
        $result = $stmt->execute();
        if ($result == false)
            return 0;
        else
            return $this->db->lastInsertId();
    
    }
    
    public function getHistoryByAlertId($alertID){
       
        $sql = "SELECT * FROM web_alerts_history ah LEFT JOIN admin_user au ON ah.user_id=au.id WHERE ah.alertid=:alertID ORDER BY ah.id DESC";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':alertID', $alertID);
        $stmt->execute();
        $result = $stmt->fetchAll();
      
        return $result;
    }
    
    public function getLastHistoryById($historyId){
        $sql = "SELECT * FROM web_alerts_history ah LEFT JOIN admin_user au ON ah.user_id=au.id WHERE ah.id=:historyId";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':historyId', $historyId);
        $stmt->execute();
        $result = $stmt->fetchAll();
      
        return $result;
    }
    
    
}
