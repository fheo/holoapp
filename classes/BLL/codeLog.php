<?php

namespace BLL;

use DAL;
require_once 'BaseClass.php';
require_once DIR_DAL . 'codeLog.php';
require_once 'Mailer.php';

class codeLog extends BaseClass {

    public $db;
    public $UID;
    public $ipAddr1;
    public $ipAddr2;
    public $mobile;
    public $userId;
    public $url;
    public $createdOn;
    
   
    
    
    public function __construct($db) {
        $this->db = $db;
            $this->UID = NULL;
            $this->ipAddr1 = NULL;
            $this->ipAddr2 = NULL;
       		$this->mobile = NULL;
            $this->userId = NULL;
            $this->url = NULL;
            $this->createdOn = NULL;
    }
    

    
    public function addCodeLog() {

        $codeLogObject = new DAL\codeLog($this->db);
        $result = $codeLogObject->addCodeLog($this->UID,$this->ipAddr1, $this->ipAddr2, $this->mobile, $this->userId,$this->url);
        return $result;
    }
    

}
