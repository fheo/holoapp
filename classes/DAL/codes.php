<?php

namespace DAL;

use DAL;
use PDO;
require_once DIR_DAL . 'BaseClass.php';
require_once DIR_CLASSES . 'encryption/AES.php';

class codes extends BaseClass {
	
	
	
	public function getCodeByOID($OID) {
		$sql = "SELECT * FROM code WHERE oid=:OID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':OID', $OID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
	}
	
	public function getCodeByUID($UID) {
		$sql = "SELECT * FROM code WHERE uid=:UID";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':UID', $UID);
		$stmt->execute();
		$result = $stmt->fetchAll();
		return $result[0];
	}
	public function getCodesByRollNumber($rollNumber) {
			$sql = "SELECT * FROM code WHERE roll_number=:rollNumber";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':rollNumber', $rollNumber);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
	}
	
	public function getCodesByCustomerName($customerName) {
		$sql = "SELECT * FROM code WHERE customer_name=:customerName";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':customerName', $customerName);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
	}
	
	public function addCode($aValues,$totalLabels) {
	
		$datafields = "oid,uid,roll_number,customer_name,is_active,is_valid,created_on";
		function placeholders($text, $count=0, $separator=","){
			$result = array();
			if($count > 0){
				for($x=0; $x<$count; $x++){
					$result[] = $text;
				}
			}
		
			return implode($separator, $result);
		}
		$this->db->beginTransaction(); // also helps speed up your inserts.
		$insert_values = array();
		for($i=0;$i<$totalLabels;$i++){
			$question_marks[] = '('  . placeholders('?', 7) . ')';
			//$insert_values = array_merge($insert_values, array_values($d));
		}
		
		$sql = "INSERT INTO code (" . $datafields  . ") VALUES " . implode(',', $question_marks);
	
		
		$stmt = $this->db->prepare($sql);
		try {
			$stmt->execute($aValues);
		} catch (PDOException $e){
			echo $e->getMessage();
		}
		$this->db->commit();
		//$pdo->commit();
		unset($aValues);
		
		/*
		
			$size = 30000;
			$array_chunk = array_chunk($aValues, $size);
			foreach($array_chunk as $values){
			$sql = 'INSERT INTO code(oid,uid,roll_number,customer_name,is_active,is_valid,created_on) VALUES '.implode(',',$values).';';
			//$sql = 'INSERT INTO code(oid,uid,roll_number,customer_name,is_active,is_valid,created_on) VALUES $values';
			$stmt = $this->db->prepare($sql);
			$result = $stmt->execute();
			unset($aValues);
			}
			
			*/
		}
	
/*	
	public function addCode($UID,$rollNumber,$customerName,$isActive,$isValid) {
        $sql = "INSERT INTO code(uid,roll_number,customer_name,is_active,is_valid,created_on) VALUES(:uid,:rollNumber,:customerName,:isActive,:isValid,now())";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':uid', $UID);
        $stmt->bindParam(':rollNumber', $rollNumber);
        $stmt->bindParam(':customerName', $customerName);
        $stmt->bindParam(':isActive', $isActive);
        $stmt->bindParam(':isValid', $isValid);
        $result = $stmt->execute();
        if ($result == false)
            return 0;
        else
            return array("UID"=>$UID,"OID"=>$this->db->lastInsertId());
	
	}
	
	*/
	
	public function getLastRollNumber(){
		$sql = "SELECT max(roll_number) FROM code";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result[0];
        
	
	}
	
	public function getAllCodes(){
		$sql = "SELECT uid FROM code";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        return $result;
	}
	
	public function getLastOID(){
		$sql = "SELECT max(oid) FROM code";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result[0];
	}
	
	public function activateCodesByRollNumber($rollNumber) {
	 
        $sql = "UPDATE code SET is_active = 1 WHERE roll_number = :rollNumber";
		$stmt = $this->db->prepare($sql);
        $stmt->bindParam(':rollNumber', $rollNumber);
        return $stmt->execute();
	
	}
	
	public function setCodeInvalid($UID) {
	    $sql = "UPDATE code SET is_valid = 0 WHERE uid = :UID";
		$stmt = $this->db->prepare($sql);
        $stmt->bindParam(':UID', $UID);
        return $stmt->execute();
	}
	
	public function assignCustomerByRollNumber($rollNumber, $customerName){
		
		
		$sql = "UPDATE code SET customer_name=:customerName WHERE roll_number=:rollNumber ";
		$stmt = $this->db->prepare($sql);
		$stmt->bindParam(':customerName', $customerName);
		$stmt->bindParam(':rollNumber', $rollNumber);
		return $stmt->execute();
		
	}
	
	    
}
