<?php

//echo 'called 6';

namespace BLL;

//echo 'called 7';
use DAL;

error_reporting(E_ALL);
//echo 'called 8';
//use DAL\ParentDetail\getParentDetails;
//require_once('../constants.php');
//echo 'ORder master path:'.$_SERVER['DOCUMENT_ROOT'];
//require_once(dirname(dirname(dirname( __FILE__ ) )).'\constants.php');
//require_once('../constants.php');
require_once DIR_BLL . 'BaseClass.php';
//require_once 'BaseClass.php';
//require_once(DIR_BASE .'db_credentials.php');
require_once DIR_DAL . 'alert.php';

//require_once './classes/DAL/order.php';
//echo 'called 9';
class Alert extends BaseClass {

    public $db;
    public $alertID;
    public $storeID;
    public $storechainID;
    public $messageID;
    public $userID;
    public $productID;
    public $productCode;
    public $cartID;
    public $orderID;
    public $createdOn;
    public $modifiedOn;
    public $historyID;
    public $status;
    public $comments;
    public $historyId;
    public $startDate;
    public $endDate;
    public $stores;

    public function __construct($db, $alertID = null) {
        error_reporting(E_ERROR);
        $this->db = $db;

        if ($alertID != NULL) {
            $alert = new DAL\Alert($this->db);
            $data = $alert->getAlert();
            $details = $data[0];
        	$this->storeID = $details['store_id'];
            $this->storeChainID = $details['storechain_id'];
            $this->messageID = $details['message_id'];
            $this->userID = $details['user_id'];
            $this->productID = $details['product_id'];
            $this->productCode = $details['product_code'];
            $this->cartID = $details['cart_id'];
            $this->orderID = $details['order_id'];
            $this->createdOn = $details['createdOn'];
            $this->modifiedOn = $details['modifiedOn'];
            $this->historyID = $details['history_id'];
            $this->status = $details['status'];
        }
        else{
        	$this->alertID = NULL;
        	$this->storeID = NULL;
        	$this->storeChainID = NULL;
        	$this->messageID = NULL;
        	$this->userID = NULL;
        	$this->productID = NULL;
        	$this->productCode = NULL;
        	$this->cartID = NULL;
        	$this->orderID = NULL;
        	$this->createdOn = NULL;
        	$this->modifiedOn = NULL;
        	$this->historyID = NULL;
        	$this->status = NULL;
        	
        }
        
    }
   
    public function getAlert() {
        $alert = new DAL\Alert($this->db);
        return $alert->getAlert($this->startDate,$this->endDate,$this->stores);
         
    }
    
    public function addHistory($userID,$alertID,$comments,$status){
        $alert = new DAL\Alert($this->db);
        $returnVal = $alert->addHistory($this->userID, $this->alertID, $this->comments, $this->status);
        if ($returnVal != 0) {
            $this->historyId = $returnVal;
        } else
            $this->historyId = 0;
        return $this->historyId;
    }
    
    public function getAlertsById(){
        $alert = new DAL\Alert($this->db);
        return  $alert->getAlertsById($this->alertID);
       
    }
    
    public function getHistoryByAlertId(){
        $alert = new DAL\Alert($this->db);
        return  $alert->getHistoryByAlertId($this->alertID);
        
    }
    
    public function updateAlertStatus(){
       
        $alert= new DAL\Alert($this->db);
        return $alert->updateAlertStatus($this->alertID,$this->status);
    }
    
    public function getLastHistoryById(){
        $alert = new DAL\Alert($this->db);
        //echo $this->historyId;
        return  $alert->getLastHistoryById($this->historyId);
    }
    
    public function createAlert(){
    	$alert = new DAL\Alert($this->db);
    	$result = $alert->createAlert($this->storeID, $this->storeChainID, $this->messageID, $this->userID,$this->productID,$this->productCode, $this->cartID,$this->orderID,$this->status);
    	$to      = "vikas@kayeura.com;";
    	$subject = 'Alert was created for store '.$storeID;
    	// To send HTML mail, the Content-type header must be set
    	$headers  = 'MIME-Version: 1.0' . "\r\n";
    	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    	
    	// Additional headers
    	$headers .= 'From: Kayeura Technologies <happiness@kayeura.com>' . "\r\n";
    	
    	$message = $html;
    	mail($to, $subject, $message, $headers);
    	return $result;
    }
    
}

    
