<?php
error_reporting(E_ERROR);
//require_once 'common.php';
require_once 'constants.php';
require_once 'masterConnection.php';
require_once 'functions.php';
include_once 'globals.php';
require_once DIR_BLL . 'Codes.php';

require_once 'Logger.php';
$logger = new Logger();

$numRolls = isset($_POST[globals::$P_NUM_ROLLS])?$_POST[globals::$P_NUM_ROLLS]:'';
$numLabelsPerRoll = isset($_POST[globals::$P_NUM_LABELS_PER_ROLL])?$_POST[globals::$P_NUM_LABELS_PER_ROLL]:'';
$customerName =  isset($_POST[globals::$P_CUSTOMER_NAME])?$_POST[globals::$P_CUSTOMER_NAME]:'';

$inputs = array("numRolls"=>$numRolls,"numLabelsPerRoll"=>$numLabelsPerRoll,"customerName"=>$customerName);
$result =checkInputs($inputs);

if ($result["error"]) {

	$logger->logEvent('generateCodes', $dataReceived, array('ERROR'=>'F|'.$result["msg"].' field(s) are required.'), 'ERROR_INVALID_INPUT');
	echo 'F|'.$result["msg"].' field(s) are required.';
	die();
}

$start = microtime(true);

$totalLabels = $numRolls*$numLabelsPerRoll;

$codeObject = new BLL\Codes($db);
$rollNumber = $codeObject->getLastRollNumber() + 1;
$oid = $codeObject->getLastOID();
$existingRandom = $codeObject->getAllCodes();
$random = array();
$aValues = array();
$newCodesArray = array();
$j=0;
$isActive = 0;
$isValid = 1;
$time_start = microtime(true);
echo 'a'.(microtime(true) - $time_start)."<br>";
for($i =0; $i<$totalLabels; $i++){
$random[] = uniqid().mt_rand(10,99);
}
echo 'b'.(microtime(true) - $time_start)."<br>";
var_dump($random);
echo 'c'.(microtime(true) - $time_start)."<br>";
/*
for($i =0; $i<$totalLabels; $i++){
	do{
	$tempRandom = mt_rand(100000000000,999999999999);
	} while(!(in_array($tempRandom, $existingRandom)) && !(in_array($tempRandom, $random) ));
	$random[] = $tempRandom;
	$oid++;
	if($j == $numLabelsPerRoll){$rollNumber++; $j =0;}
		array_push($aValues,$oid, $row, $rollNumber,$customerName,$isActive,$isValid,date('Y-m-d H:i:s'));
		$newCodesArray[] = array($row,$oid,$rollNumber,$customerName);
		$j++;
}
echo 'a'.(microtime(true) - $time_start)."<br>";
	
unset($existingRandom);
unset($random);
echo 'b'.(microtime(true) - $time_start)."<br>";
$codeObject->addCode($aValues,$totalLabels);
$fileName = $customerName."_scratchcodes_" . date('Ymdhs') . ".csv";
convert_to_csv($newCodesArray,$fileName,',');
unset($newCodesArray);
unset($codeObject);
unset($aValues);
echo 'c'.(microtime(true) - $time_start)."<br>";
 




function convert_to_csv($input_array, $fileName, $delimiter)
{
	
    header('Content-Type: text/csv');
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	/* open raw memory as file, no need for temp files, be careful not to run out of memory thought *
	$f = fopen('php://output', 'w');
	/* loop through array  *
	foreach ($input_array as $line) {
		/* default php csv handler *
		
		fputcsv($f, $line, $delimiter);
	}
	fclose($f) or die("Can't close php://output");

}

*/



