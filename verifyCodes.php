<?php
error_reporting(E_ERROR);
//require_once 'common.php';
require_once 'constants.php';
require_once 'masterConnection.php';
require_once 'functions.php';
include_once 'globals.php';
require 'vendor/autoload.php';
require_once DIR_BLL . 'Codes.php';

require_once 'Logger.php';
$logger = new Logger();

/* Variables being received from mobile
 *
 */

$inNumber = isset($_POST[globals::$P_IN_NUMBER])?$_POST[globals::$P_IN_NUMBER]:'';
$mobilenum = isset($_POST[globals::$P_SENDER])?$_POST[globals::$P_SENDER]:'';
$keyword = isset($_POST[globals::$P_KEYWORD])?$_POST[globals::$P_KEYWORD]:'';
$content = trim(isset($_POST[globals::$P_CONTENT])?$_POST[globals::$P_CONTENT]:'');
$email = isset($_POST[globals::$P_EMAIL])?$_POST[globals::$P_EMAIL]:'';
$credits = isset($_POST[globals::$P_CREDITS])?$_POST[globals::$P_CREDITS]:'';

$data   = preg_split('/\s+/', $content);
if(count($data)> 1){
	$uid = $data[1];
	$keyword = $data[0];
}else{
	$uid = $data[0]	;
}


// Check if variables are not empty
$inputs = array("mobilenum"=>$mobilenum,"uid"=>$uid);
$result =checkInputs($inputs);
if ($result["error"]) {
	echo 'F|'.$result["msg"].' field(s) are required.';
	die();
}

$dataReceived = array(
		globals::$P_CONTENT => $content,
		globals::$P_SENDER => $mobilenum,
		
);
$codeObject = new BLL\Codes($db);
$codeObject->UID = $uid;
$result = $codeObject->getCodeByUID();
if(!empty($result)){
	if($result["is_active"] ==1){
		if($result["is_valid"] == 1){
			//$message =  $uid." is a valid code. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
			$message =  "This is a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
			$codeObject->setCodeInvalid();
		}
		elseif($result["isValid"] == 0){
			//$message =  $uid." was a valid code but isn't valid anymore . Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
			$message =  "This is not a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
		}
		
	}else{
		//$message =  $uid." code is valid but hasn't been activated yet. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
		$message =  "This is not a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
	}
	
}else{
	//$message =  $uid." is an invalid code. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system";
	$message =  "This is not a genuine product. Thank you for using Holosecurity Technologies'(www.holosecurity.com) verification system.";
}

require_once './vendor/plivo/plivo.php';
$p = new RestAPI(PLIVO_AUTH_ID, PLIVO_AUTH_TOKEN);


	// Set message parameters
$params = array(
			'src' => '1111111111', // Sender's phone number with country code
			'dst' => $mobilenum, // Receiver's phone number with country code
			'text' => $message // Your SMS text message
			// To send Unicode text
			//'text' => 'こんにちは、元気ですか？' # Your SMS Text Message - Japanese
			//'text' => 'Ce est texte généré aléatoirement' # Your SMS Text Message - French
			//'url' => 'http://example.com/report/', // The URL to which with the status of the message is sent
			//'method' => 'POST' // The method used to call the url
	);
	// Send message
	$response = $p->send_message($params);


	/*Log OTP Generation response here
	 * Code to be written
	 * echo "Response : ";
	 print_r ($response['response']);

	 // Print the Api ID
	 echo "<br> Api ID : {$response['response']['api_id']} <br>";

	 // Print the Message UUID
	 echo "Message UUID : {$response['response']['message_uuid'][0]} <br>";
	 */


	//Code checks if there was any error during response generation
	if(array_key_exists('error', $response['response'])){
		if($response['response']['error']!='') {
			if(strpos($response['response']['error'],'is not a valid phone number') !== false){
				$logger->logEvent('signup', $dataReceived, array('ERROR'=>'F|Message Could not be sent. Please check your number.'.'<br>'.$response['response']['error']), 'ERROR_SENDING_OTP');
				echo 'F|Message Could not be sent. Please check your number.'.'<br>';
				echo 'Response Code: '.$response['response']['error'];
				exit();
			}
			echo 'F|Message Could not be sent. Please check your number or try again after sometime.';
			echo 'Response Code: '.$response['response']['error'];
			$logger->logEvent('signup', $dataReceived, array('ERROR'=>'F|Message Could not be sent. Please check your number or try again after sometime.'.$response['response']['error']), 'ERROR_SENDING_OTP');
			exit();
		}
	}
	
	unset($codeObject);
	unset($result);
	


