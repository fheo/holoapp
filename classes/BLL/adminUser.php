<?php

namespace BLL;

use DAL;
require_once 'BaseClass.php';
require_once DIR_DAL . 'adminUser.php';
require_once 'Mailer.php';

class AdminUser extends BaseClass {

    public $db;
    public $userID;
    public $username;
    public $password;
    public $firstName;
    public $lastName;
    public $email;
    public $storeID;
    public $storeChainID;
    public $roleID;
    public $isActive;
    public $isAdmin;
    public $createdOn;
    public $modifiedOn;
    public $permissionArray;
    
    
    public function __construct($db, $userID = null) {
        $this->db = $db;
        if ($userID != NULL) {
            $user = new DAL\AdminUser($this->db);
            $data = $user->getUser($userID);
            $details = $data[0];            
            $this->userID = $details['id'];
            $this->username = $details['username'];
            $this->password = $details['password'];
            $this->firstName = $details['first_name'];
            $this->lastName = $details['last_name'];
            $this->email = $details['email'];
            $this->storeID = $details['store_id'];
            $this->storeChainID = $details['store_chain_id'];
            $this->roleID = $details['role_id'];
            $this->isActive = $details['is_active'];
            $this->isAdmin = $details['is_admin'];
            $this->createdOn = $details['created_on'];
            $this->modifiedOn = $details['modified_on'];
            
        } else {
            $this->userID = NULL;
            $this->username = NULL;
            $this->password = NULL;
            $this->firstName = NULL;
            $this->lastName = NULL;
            $this->email = NULL;
            $this->storeID = NULL;
            $this->storeChainID = NULL;
            $this->roleID = NULL;
            $this->isActive = NULL;
            $this->isAdmin = NULL;
            $this->createdOn = NULL;
            $this->modifiedOn = NULL;
        }
    }
    
    public function getUserByCredentials() {
        $user = new DAL\AdminUser($this->db);
        return $user->getUserByCredentials($this->username, $this->password);
    }

    public function getByUsername() {
        $user = new DAL\AdminUser($this->db);
        return $user->getByUsername($this->username);
    }
    
    public function getUserById() {
        $user = new DAL\AdminUser($this->db);
        return $user->getUser($this->userID);
    }
    
    public function getUserDashboardPermission() {
        $user = new DAL\AdminUser($this->db);
        return $user->getDashboardPermission($this->userID);
    }

    public function addUser() {
        $user = new DAL\AdminUser($this->db);
        $result = $user->addUser($this->username,$this->password,$this->firstName,$this->lastName,$this->email,$this->storeID,$this->storeChainID,$this->roleID,$this->isActive,$this->isAdmin);
        
        $last_user_id = $result[0];
        
        $result1=$user->addPermission($this->permissionArray,$last_user_id);
        
       return $result;
        
    }
    
    public function getDashboardPermission(){
        $user = new DAL\AdminUser($this->db);
        $result =$user->getDashboardPermission($this->userID);
        return $result;
    }
    
    public function resetPassword($password, $hash)
    {
        
        $userObj = new DAL\AdminUser($this->db);
                
        $hashedPresent = $userObj->getHashedDetails($hash);
        
        if(empty($hashedPresent))
        {
           return 0;
        }  
        return $userObj->resetAdminUsersPassword($hashedPresent[0]['userid_fk'], $password, $hashedPresent[0]['id']);
    }
    
    public function resetPasswordLink($userID){
        
        
        
        $userObj = new DAL\AdminUser($this->db);
        //check if anything previous present if so delete it
        
        $hashedPresent = $userObj->getAdminHashedLink($userID);
      
        if(!empty($hashedPresent))
        {
            //delete it and create a new request
            $userObj->deleteHashedLink($hashedPresent[0]['id']);
        }
        
        $matchingString = $this->generateString($userID);      
        $userObj->resetPasswordLink($userID, $matchingString);
        $userDetails = $userObj->getUser($userID);
        
        $hashLink = DIR_QSKIP. "admin/resetPassword.php?hash=".$matchingString;
        $from='admin@kayeura.com';
         $subject='Password Reset Link';
        
        $body = ""
                . "Dear ".$userDetails[0]['username'].","
                . "         You password reset link is ".$hashLink."\r\n"
                . "Regards,\r\n"
                . "Team Kayeura"
                . "";
        
                
        $mailer = new Mailer();
        return $mailer->sendMail($from, $userDetails[0]['email'], $subject, $body);
        
    }
    
    
    
    
    private function generateString($str)
    {
        return md5($str.uniqid());
    }
    
    public function getAllAdminUsers(){
        $user = new DAL\AdminUser($this->db);
        return $user->getAllAdminUsers();
    }
    
    public function updateUserByID(){
        $user = new DAL\AdminUser($this->db);
        $result = $user->updateUserByID($this->userID,$this->firstName,$this->lastName,$this->email);
        
        if(!empty($this->permissionArray))
        {
            $result = $user->updatePermission($this->userID, $this->permissionArray);
        }
        return $result; 
        
    }
    
    public function deleteUserByID(){
        $user = new DAL\AdminUser($this->db);
        return $user->deleteUserByID($this->userID);
    }
    
}
