<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BLL;

require_once 'BaseClass.php';


class Mailer  extends BaseClass{
   
    private $URL = "https://api.sendgrid.com/api/mail.send.json";
    private $API_KEY = "easybiz123";
    private $API_USER = "kayeura";
    
    public function sendMail($from , $to, $subject, $body )
    {
        $header = array();
        
        if(empty($from))
        {
            $from = "info@kayeura.com";
        }
        
        $postvars = array(
            'api_user' => $this->API_USER,
            'api_key' => $this->API_KEY,
            'to' => $to,
            'subject' => $subject,
            'from' => $from,
            'text' => $body
        );
        
        $dataArray = json_decode($this->execute($this->URL, $header, $postvars), true);
        
        return ($dataArray['message'] == 'success');
        
    }
    
    private function execute($URL, $header = array(), $postvars = array())
   {
        $ch = curl_init();  
        curl_setopt($ch,CURLOPT_URL,$URL);
        
        
        if(!empty($header))
        {
           curl_setopt($ch, CURLOPT_HEADER, $header);
        }
        
        if(!empty($postvars))
        {
            curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
            curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
        }else{
            curl_setopt($ch,CURLOPT_POST, 0);                //0 for a get request
        }
        
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        return $response;
    }
}
