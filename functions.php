<?php

function getProductRecommendation($productID,$storeID) {
    global $db;
    $sql = "select product_id from product_store_detail where store_id=:storeID and product_id<>:productID order by rand() limit 2";
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':storeID', $storeID);
    $stmt->bindParam(':productID', $productID);
    $stmt->execute();
    $result = $stmt->fetchAll();
    $recommendation='';
    foreach($result as $obj)
    {
        $recommendation.=$obj['product_id'].'`';
    }
    return rtrim($recommendation,'`');
//    return $result['recommendation'];
//    $sql = "select group_concat(recommended_product_id separator '`') recommendation from product_recommendation where product_id=:pid";
//    $stmt = $db->prepare($sql);
//    $stmt->bindParam(':pid', $productID);
//    $stmt->execute();
//    $result = $stmt->fetchAll();
//    return $result[0]['recommendation'];
}



//function to create HTML Bill based on order or cart details
function getHtmlBill($details){
	
	$html = "
	
<!DOCTYPE html>
<html lang='en-US'>
<head>
	
</head>
	
<body>
";
	
	$html .= "<table width='100%' cellpadding='2' cellspacing='0' border='0'><tr style='font-family:Roboto-Regular,Arial,Helvetica,sans-serif;'><th style='text-align:left'>Item</th><th style='text-align:right'>Amount (&#8377;)</th><tr>";
	$html .= "<tr bgcolor='lightgrey'><td colspan='2'><tr>";
	foreach ($details['lineItems'] as $orderline) {
		$html .= '<tr style="font-family:Roboto-Regular,Arial,Helvetica,sans-serif;"><td>'.$orderline['Name']. '('.$orderline['Quantity'].'@' . number_format($orderline['Price'], 2) . ')</td><td align="right">' .  $orderline['FinalPrice'] . '</td><tr>';
		if($orderline['OfferName'] != null){
			
			$html .= '<tr style="font-family:Roboto-Regular,Arial,Helvetica,sans-serif;"><td style="font-size:small;font-style: italic;color: grey;">' . $orderline['OfferName'] . ' | Saved:&#8377;' . $orderline['Discount'] . '</td></tr>';
		}	
	
	}
	$html .= "<tr bgcolor='lightgrey'><td colspan='2'><tr>";
	$html .= "<tr style='font-family:Roboto-Regular,Arial,Helvetica,sans-serif;'><td><b>Grand Total</b></td><td align='right'><b>" . number_format($details['GrandTotalWithDiscount'], 2) . "</b></td><tr>";
	
	if($details[BillOffer]!=null)
	{
		$html .= '<tr style="font-family:Roboto-Regular,Arial,Helvetica,sans-serif;"><td style="font-size:small;font-style: italic;color: grey;">' . $details['BillOfferName'] . ' | Saved:&#8377;' . $details['BillOffer'] . '</td></tr>';
	}
	
	$html .= "<tr><td colspan='2'></td></tr>";
	$html .= "<tr style='font-family:Roboto-Regular,Arial,Helvetica,sans-serif;'><td>  <b>Your Total VAT is</b></td><td align='right'><b>" . number_format($details['TotalVatAmount'], 2) . "</b></td><tr>";
	
	$html .= "</table>";
	
	$html .= "
</body>
	
</html>
	
";
	
return $html;	
	
}




        

function getBillToPrintJSON($details, $storeDetails){
	

	$billToPrint['store'] = array(
            'name' => $storeDetails['name'],
            'address1' => $storeDetails['address1'],
            'address2' => $storeDetails['address2'],
            'zipcode'  => $storeDetails['zipcode'],
        );
        
	foreach ($details['lineItems'] as $orderline) {
            
                $billToPrint['items'][] = array(
                    'name' => $orderline['Name'],
                    'quantity' => $orderline['Quantity'],
                    'price' => number_format($orderline['Price'], 2),
                    'finalPrice' => $orderline['FinalPrice'],
                    'offer' => $orderline['OfferName'],
                    'discount' => $orderline['Discount']
                );
	}
	
        $billToPrint['grandTotal'] = number_format($details['GrandTotalWithDiscount'], 2);
	$billToPrint['billOfferName'] = $details['BillOfferName'];
        $billToPrint['billOffer'] = $details['BillOffer'];
	$billToPrint['totalVatAmount'] = number_format($details['TotalVatAmount'], 2);
	
	return $billToPrint;
	
}

function getBillToPrint($details){
	
	
	$billToPrint = "";
	
        
	$billToPrint .= "Item          Amount()"."\n";
	foreach ($details['lineItems'] as $orderline) {
		$billToPrint .= $orderline['Name']. '('.$orderline['Quantity'].'@' . number_format($orderline['Price'], 2) . ')' .'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'.  $orderline['FinalPrice']."\n";
		if($orderline['OfferName'] != null){
				
			$billToPrint .= $orderline['OfferName'] . ' | Saved:&#8377;' . $orderline['Discount']."<br>";
		}
	
	}
	$billToPrint.="________________________________________"."<br>";
	$billToPrint .= "<b>Grand Total</b>" . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'.number_format($details['GrandTotalWithDiscount'], 2)."<br>";
	
	if($details[BillOffer]!=null)
	{
		$billToPrint .= $details['BillOfferName'] . ' | Saved:&#8377;' . $details['BillOffer']."<br>";
	}
	
	
	$billToPrint .="<b>Your Total VAT is</b>" . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'.number_format($details['TotalVatAmount'], 2)."<br>";
	$billToPrint.="________________________________________";
	
	return $billToPrint;
	
	
}


function getTimestamp() {
    date_default_timezone_set('Asia/Kolkata');
    list($usec, $sec) = explode(' ', microtime()); //split the microtime on space
    //with two tokens $usec and $sec

    $usec = str_replace("0.", ".", $usec);     //remove the leading '0.' from usec

    $timestamp = date('YmdHis', $sec) . $usec;       //appends the decimal portion of seconds
    return $timestamp;
}

function searchInArray($val, $key, $array)
{
    if(count($array)==0)
        return FALSE;
    $exists = FALSE;
    foreach($array as $element)
    {
        if(array_key_exists($key, $element))
        {
            if($element[$key]==$val)
            {
                $exists = TRUE;
                return $exists;
            }            
        }
    }
    return FALSE;
}

function dateFormat($inputDate, $formatType)
{
    $outputDate='';
    if($formatType==1)//'Y-m-d'
    {
        $outputDate = date('Y-m-d',strtotime($inputDate));
    }
    else if($formatType==2)//'d/m/Y'
    {
        $outputDate = date('d/m/Y',strtotime($inputDate));
    }
    return $outputDate;
}

function getStateByCountry($countryID) {
    global $db;
    require_once DIR_BLL . 'state.php';
    $state = new BLL\State($db);
    $state->countryID = $countryID;
    $arrState = $state->getStateByCountry();
    return $arrState;    
}

function getCustomerID() {
    global $db;
    require_once DIR_BLL . 'user.php';
    $user = new BLL\User($db);
    $user->customerID = mt_rand(10000000,99999999);
    $result = $user->getUserByCustomerID();
    if(count($result)>0)
    {
        return getCustomerID();
    }
    else
    {
        return $user->customerID;
    }
}

function getCitiesByState($stateID) {
    global $db;
    require_once DIR_BLL . 'city.php';
    $city = new BLL\City($db);
    $city->stateID = $stateID;
    $arrCity = $city->getCityByState();
    return $arrCity;    
}
function getStoresByCity($cityID) {
    global $db;
    require_once DIR_BLL . 'store.php';
    $store = new BLL\Store($db);
    $store->cityID = $cityID;
    $arrStore = $store->getStoreByCity();
    return $arrStore;
}
function getPercentage($current,$previous)
{
    $percentage = 0;
    if($current==$previous)
    {
        $percentage=0;
    }
    else if($current>$previous)
    {
        if($previous==0)
        {
            $percentage=100;
        }
        else
        {
            $percentage=round($current/$previous,2);
        }
    }
    else if($previous>$current)
    {
        if($current==0)
        {
            $percentage=-100;
        }
        else
        {
            $percentage = -round($previous/$current,2);
        }
    }
    return $percentage;
}

function checkInputs($inputs){
    $error = false;
    $msg = '';
    foreach($inputs as $inputname=>$inputvalue){
        if (empty($inputvalue)) {
        $error = true;
            if (empty($msg)){
                 $msg = $inputname;
             }
             else {
                 $msg .= ', '.$inputname;
             }
        }
    }
    return array("error"=>$error,"msg"=>$msg);
}


function convertLocalToUTC($localDate,$localTime){
	
	$date = explode("-",$localDate);
	$time = explode(":",$localTime);

	$utcDate = new DateTime();
	$utcDate->setTimezone(new DateTimeZone('Asia/Kolkata'));
	$utcDate->setDate($date[0],$date[1],$date[2]);
	if(count($time)==3)
    {    
    $utcDate->setTime($time[0],$time[1],$time[2]);
    }else{
    $utcDate->setTime($time[0],$time[1],'00');    
    }
	$utcDate->setTimezone(new DateTimeZone('UTC'));
	return $utcDate->format('Y-m-d H:i:s');
}


function convertUTCToLocal($utcDateTime){
list($utcDate,$utcTime) = explode(" ",$utcDateTime);
	$date = explode("-",$utcDate);
	$time = explode(":",$utcTime);

	$localDate = new DateTime();
	$localDate->setTimezone(new DateTimeZone('UTC'));
	$localDate->setDate($date[0],$date[1],$date[2]);
    if(count($time)==3)
    {    
    $localDate->setTime($time[0],$time[1],$time[2]);
    }else{
    $localDate->setTime($time[0],$time[1],'00');    
    }
	
	$localDate->setTimezone(new DateTimeZone('Asia/Kolkata'));
	return $localDate->format('Y-m-d H:i:s');
}