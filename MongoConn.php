<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mongodb
 *
 * @author Kayeura
 */
class MongoConn {
    
    private  $URI = "mongodb://kayeura:Easybiz12#@ds062438.mongolab.com:62438/" , 
             $moDB, $moDBName = 'MongoDB-x', $selectedDB; 
    
    public function connection(){
        
        if(empty($this->selectedDB))
        {
           try{
               $this->moDB = new MongoClient($this->URI.$this->moDBName);
               $this->selectedDB = $this->moDB->selectDB($this->moDBName);
           }
           catch(Exception $e){
               echo $e->getMessage()."<br>";
           }
           
        }
        return $this->selectedDB;
        
    }
    
    
    /**
     * Function to insert data into collection
     * 
     * @param string $collectionName
     * @param json encoded string $document
     * @return array (1,inserted) if success / (-1, errormsg) if fails
     */
    public function insertData($collectionName, $document)
    {   
        try{
        
        //Create a db connection
        $db = $this->connection(); 
        
        //Select collection name
        $col = $db->$collectionName;
     
        //insert document
         $col->insert($document);
         
         //no error return 1;
         return array(1, 'inserted');
         
        }catch(Exception $e)
        {
            //Soemthing went wrong
            return array(-1, $e->getMessage());
        }
        
    }
    
    
    
    public function findAndModify($collectionName, $where, $update, $return, $options)
    {
        try{
        
        //Create a db connection
        $db = $this->connection(); 
        
        $col = $db->$collectionName;
        
        $returnVal = $col->findAndModify($where, $update, $return, $options);
        
         //no error return 1;
         return array(1, $returnVal);
         
        }catch(Exception $e)
        {
            //Soemthing went wrong
            return array(-1, $e->getMessage());
        }
    }
    
    
    
    
    /**
     * Function to get all documents
     * 
     * @param string $collectionName
     * @return array (1,inserted) if success / (-1, errormsg) if fails
     */
    public function getAllDocuments($collectionName){
        
        try{
        
        //Create a db connection
        $db = $this->connection(); 
       
        //Select colelction name
        $col = $db->$collectionName;
      
        //find document
         $documents =  $col->find();
         
         //no error return 1;
         return array(1, $documents);
         
        }catch(Exception $e)
        {
            //Soemthing went wrong
            return array(-1, $e->getMessage());
        }
        
    }
    
    /*************************************
    * Function to count number of documents count
    *  
    * $where condition is the one which gives the condition
    * 
      ************************************/
    public function countDocuments($collectionName, $columns, $where = array(), $sort = array(), $limit = 0) {
         try{
             //$this->dieArray($where);
                $db = $this->connection(); 

                
                $col = $db->$collectionName;

               
                $documents =  ($col->count($where));
                 
                return array(1, $documents);
         
                }catch(Exception $e)
                {
                    //Soemthing went wrong
                        return array(-1, $e->getMessage());
                }   
            }
            
            
    /*************************************
    * Function to aggregate pipelined values by query
    *  
    * $where condition is the one which gives the condition
    * 
      ************************************/
            

    public function aggregateDocument($collectionName, $columns, $pipeline = array(), $sort = array(), $limit = 0) {
         
            try {
              
                    $db = $this->connection();
                    $col = $db->$collectionName;
               
                    return $col->aggregate($pipeline); 
   
                }
             catch (Exception $ex) {
                 return array(-1, $ex->getMessage());
                }
                
   }    
            
            
            
       
    public function selectData($collectionName, $columns, $where = array(), $sort = array(), $limit = 0){
        
        try{
                //Create a db connection
                $db = $this->connection(); 

                //Select colelction name
                $col = $db->$collectionName;

                if(!empty($sort) && !empty($limit))
                {
                    //Select Query
                    $documents =  $col->find($where, $columns)->sort($sort)->limit($limit);
                    
                }else{
                    
                    //Select Query
                    $documents =  $col->find($where, $columns);
                    
                }

                 //no error return 1;
                 return array(1, $documents);
         
        }catch(Exception $e)
        {
            //Soemthing went wrong
            return array(-1, $e->getMessage());
        }
        
        
    }
    
    
    /**
     * Only for development purposes
     */
    public function dieArray($dataArray) {
        echo "<pre>";
        $this->print_r($dataArray);
        die;
    }

    public function print_r($array) {
        if (__DEBUG_MODE__) {
            echo "<pre>";
            if (is_array($array)) {
                print_r($array);
            } else {
                echo strval($array);
            }
            echo "</pre>";
        }
    }

    
}
