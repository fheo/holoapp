<?php

namespace BLL;

use DAL;
require_once 'BaseClass.php';
require_once DIR_DAL . 'codes.php';
require_once 'Mailer.php';

class codes extends BaseClass {

    public $db;
    public $OID;
    public $UID;
    public $rollNumber;
    public $customerName;
    public $isActive;
    public $isValid;
    public $createdOn;
    public $modifiedOn;
   
    
    
    public function __construct($db, $OID = null) {
        $this->db = $db;
        if ($OID != NULL) {
            $codeObject = new DAL\codes($this->db);
            $data = $codeObject->getCode($OID);
            $details = $data[0];            
            $this->UID = $details['uid'];
            $this->rollNumber = $details['rollNumber'];
            $this->customerName = $details['customerName'];
            $this->isActive = $details['is_active'];
            $this->isValid = $details['is_valid'];
            $this->createdOn = $details['created_on'];
            $this->modifiedOn = $details['modified_on'];
            
        } else {
            $this->OID = NULL;
            $this->UID = NULL;
            $this->rollNumber = NULL;
            $this->customerName = NULL;
       		$this->isActive = NULL;
            $this->isValid = NULL;
            $this->createdOn = NULL;
            $this->modifiedOn = NULL;
        }
    }
    
    public function getCodeByOID() {
        $codeObject = new DAL\codes($this->db);
        return $codeObject->getCodeByOID($this->OID);
    }
    public function getCodesByRollNumber() {
    	$codeObject = new DAL\codes($this->db);
    	return $codeObject->getCodesByRollNumber($this->rollNumber);
    }
    
    public function getCodesByCustomerName() {
    	$codeObject = new DAL\codes($this->db);
    	return $codeObject->getCodesByCustomerName($this->customerName);
    }
    
    public function addCode($aValues,$totalLabels) {
    	$this->isActive = 0;
    	$this->isValid = 1;
    	 
    	$codeObject = new DAL\codes($this->db);
    	$result = $codeObject->addCode($aValues,$totalLabels);
    	//$last_OID = $result[1];
    	//$last_rollNumber = $result[3];
    	unset($codeObject);
    	return $result;
    }
    /*
    
        public function addCode() {
    	$this->isActive = 0;
    	$this->isValid = 1;
    	 
    	$codeObject = new DAL\Codes($this->db);
    	$result = $codeObject->addCode($this->OID,$this->UID,$this->rollNumber,$this->customerName,$this->isActive,$this->isValid);
    	//$last_OID = $result[1];
    	//$last_rollNumber = $result[3];
    	return $result;
    }


    public function addCode() {
    	$this->isActive = 0;
    	$this->isValid = 1;
    	
        $codeObject = new DAL\Codes($this->db);
        $result = $codeObject->addCode($this->UID,$this->rollNumber,$this->customerName,$this->isActive,$this->isValid);
        //$last_OID = $result[1];
        //$last_rollNumber = $result[3];
		return $result;
        
    }
    */
    
    public function activateCodesByRollNumber() {
    	$codeObject = new DAL\codes($this->db);
    	$result = $codeObject->activateCodesByRollNumber($this->rollNumber);
        return $result;
    }
    
    public function getLastRollNumber(){
    	$codeObject = new DAL\codes($this->db);
    	$result = $codeObject->getLastRollNumber();
    	return $result;
    }
    
    public function getLastOID(){
    	$codeObject = new DAL\codes($this->db);
    	$result = $codeObject->getLastOID();
    	return $result;
    }
    
    public function getAllCodes(){
    	$codeObject = new DAL\codes($this->db);
    	$result = $codeObject->getAllCodes();
    	return $result;
    }
    
   public function getCodeByUID() {
        
    	$codeObject = new DAL\codes($this->db);
    	$result = $codeObject->getCodeByUID($this->UID);
    	return $result;
   }
   
   public function setCodeInvalid() {
   	$codeObject = new DAL\codes($this->db);
   	$result = $codeObject->setCodeInvalid($this->UID);
   	return $result;
   }
   
   public function assignCustomerByRollNumber(){
   	
   	$codeObject = new DAL\codes($this->db);
   	$result = $codeObject->assignCustomerByRollNumber($this->rollNumber,$this->customerName);
   	return $result;
   }
    
        
}
