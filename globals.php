<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of globals
 *
 * @author sumeendranath
 */
class globals {
    //put your code here
	public static  $P_NUM_ROLLS = "numRolls";
	public static  $P_NUM_LABELS_PER_ROLL= "numLabelsPerRoll";
	public static  $P_CUSTOMER_NAME = "customerName";
	public static  $P_ROLL_NUMBER = "rollNumber";
	public static  $P_UID = "uid";
	
	
	public static $P_IN_NUMBER = "inNumber";
	public static $P_SENDER = "sender";
	public static $P_KEYWORD = "keyword";
	public static $P_CONTENT = "content";
	public static  $P_EMAIL = "email";
	public static $P_CREDITS = "credits";
	
     public static  $P_IMEI = "imei";
     public static  $P_TABLET_IMEI = "tabletimei";
    public static  $P_FIRSTNAME = "firstname";
    public static  $P_LASTNAME = "lastname";
   
    public static  $P_APIKEY = "apikey";

    public static $P_USERNAME = "username";
    public static $P_USERID = "userid";
    public static $P_PASSWORD = "password";
    public static $P_MOBILENO = "mobileno";
    public static $P_GPS = "gps";
    public static $P_STORE_QR = "storeqr";
    public static $P_STORE_ID = "storeid";
    public static $P_ORDER_ID = "orderid";
    public static $P_CART_ID = "cartid";
    public static $P_PROD_REQUEST_TYPE = "datareqtype";
    public static $P_BILL_REQUEST_TYPE = "billreqtype";
    public static $P_PAYMENT_STATUS = "paymentstatus";
    public static $P_TRANSACTION_ID = "transactionid";
    public static $P_MSG = "msg";
    public static $P_OTP_TYPE = "otptype";
    public static $P_OTP = "otp";
     public static $P_QRCODE = "qrcode";

    public static  $P_PRODUCTCODE_EXTERNAL = "productcodeexternal";
    public static  $P_PRODUCTCODE_INTERNAL = "productcodeinternal";

    public static $P_ORDER_DATA = "orderdata";
    public static $P_EVENT_DATA = "eventdata";
    
    public static $P_PAYMENT_GATEWAY_ID = "paymentgatewayid";
    public static $P_PAYMENT_GATEWAY = "paymentgateway";
    public static $P_PAYMENT_GATEWAY_TRANSACTIONID = "pgtransactionid";
    
    public static $P_PAYU_PAYMENT_TOKEN = "paytoken";
    
    public static $P_PAYU_PAYMENT_URL = "https://test.payu.in/merchant/postservice.php?form=2";
    //https://info.payu.in/merchant/postservice.php?form=2  -- for production
    
    public static $P_CHECK_USER = "checkuser";

    
}
