<?php

namespace DAL;

use DAL;

require_once DIR_DAL . 'BaseClass.php';

class User extends BaseClass
{

    public function getUser($userID)
    {
        $sql = "SELECT * FROM user where user_id=:userID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userID', $userID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    
    public function getUserFromUserRegistration($mobile){
        
        $sql = "SELECT * FROM user_registration where mobile=:mobile and is_active = 1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getUserByMobile($mobile)
    {
        $sql = "SELECT * FROM user where mobile=:mobile AND deleted = 0";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getUserByCustomerID($customerID)
    {
        $sql = "SELECT * FROM user where customer_id=:customerID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':customerID', $customerID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM user where email = :email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addUser($firstName, $lastName, $email, $password, $mobile, $imei, $customerID, $duplicateError)
    {
        $sql = "INSERT INTO user(customer_id,first_name,last_name,email,password,mobile,imei,createdOn) VALUES(:customerID,:firstName,:lastName,:email,:password,:mobile,:imei,now())";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':customerID', $customerID);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':imei', $imei);
        try {
            $result = $stmt->execute();
            if ($result == false)
                return 0;
            else
                return array($this->db->lastInsertId(), $customerID);
        } catch (\PDOException $e) {
            if ($e->errorInfo[1] == 1062 && !$duplicateError) {
                // duplicate entry, do something else
                return -1;
            } else {
                // an error other than duplicate entry occurred
                return 0;
            }
        }
    }
    
    public function updateUser($firstName, $lastName, $email, $password, $mobile, $imei)
    {
        $sql = "UPDATE user SET first_name=:firstName,last_name=:lastName,email=:email,imei=:imei WHERE mobile=:mobile";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':imei', $imei);
            return $stmt->execute();
    }
    
    public function deleteUser($userID)
    {
        $sql = "UPDATE user SET deleted = 1 WHERE user_id = :userID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userID', $userID);
        return $stmt->execute();
    }
    
    public function addUserRegistration($firstName, $lastName, $email, $password, $mobile, $imei)
    {
        $sql = "INSERT INTO user_registration(first_name,last_name,email,password,mobile,imei,createdOn,is_active) VALUES(:firstName,:lastName,:email,:password,:mobile,:imei,now(),1)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':imei', $imei);
            return $stmt->execute();
    }

    public function updatePassword($userID, $password)
    {
        $sql = "UPDATE user SET password=:password WHERE user_id=:userID ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userID', $userID);
        $stmt->bindParam(':password', $password);
        return $stmt->execute();
    }

    public function updateIMEI($userID, $imei)
    {
        $sql = "UPDATE user SET imei=:imei WHERE user_id=:userID ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userID', $userID);
        $stmt->bindParam(':imei', $imei);
        return $stmt->execute();
    }
    
    public function removeUserRegistration($mobile)
    {
        $sql = "UPDATE user_registration SET is_active=0 WHERE mobile=:mobile";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':mobile', $mobile);
        return $stmt->execute();
    }
    
    /* Code for User Details Update
     * 
     */
    
    public function addUserUpdate($userID,$firstName, $lastName, $email, $password, $mobile, $imei){
    	$sql = "INSERT INTO user_update(user_id,first_name,last_name,email,password,mobile,imei,createdOn,is_active) VALUES(:userID,:firstName,:lastName,:email,:password,:mobile,:imei,now(),1)";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userID', $userID);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':mobile', $mobile);
        $stmt->bindParam(':imei', $imei);
            return $stmt->execute();    }
    
    public function removeUserUpdate($userID)
    {
    	$sql = "UPDATE user_update SET is_active=0 WHERE user_id=:userID";
    	$stmt = $this->db->prepare($sql);
    	$stmt->bindParam(':userID', $userID);
    	return $stmt->execute();
    }
    
    public function getUserFromUserUpdate($userID){
    
    	$sql = "SELECT * FROM user_update where user_id=:userID and is_active = 1";
    	$stmt = $this->db->prepare($sql);
    	$stmt->bindParam(':userID', $userID);
    	$stmt->execute();
    	$result = $stmt->fetchAll();
    	return $result;
    }
    
    public function updateUserByID($userID, $firstName, $lastName, $email, $password, $mobile, $imei)
    {
    	$sql = "UPDATE user SET first_name=:firstName,last_name=:lastName,email=:email, mobile=:mobile,imei=:imei WHERE user_id=:userID";
    	$stmt = $this->db->prepare($sql);
    	$stmt->bindParam(':userID', $userID);
    	$stmt->bindParam(':firstName', $firstName);
    	$stmt->bindParam(':lastName', $lastName);
    	$stmt->bindParam(':email', $email);
    	$stmt->bindParam(':mobile', $mobile);
    	$stmt->bindParam(':imei', $imei);
    	return $stmt->execute();
    }
    

}
