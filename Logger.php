<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'MongoConn.php';
require_once 'constants.php';

/**
 * Description of Logger
 *
 * @author Kayeura
 */
class Logger {
    
    private  $mongo, $LOGGER_ON = FALSE;

    public function __construct() {
       
        if(empty($this->mongo) && $this->LOGGER_ON)
        {
            $this->mongo = new MongoConn();
        }
    }

    public function logEvent($event, $dataReceived, $dataSent, $actionPerformed, $eventType = 'mAPI')
    {
        if($this->LOGGER_ON)
        {
            $document = array(
                'event_type' => $eventType, 
                'event' => $event,
                'data_received' => $dataReceived,
                'data_sent' => $dataSent,
                'action_performed' => $actionPerformed, 
                'datetime' => time(),
                'datetime_DDMMYY' => date("Y-m-d H:i:s")
            );

            return $this->mongo->insertData('eventLogs', $document);
        }
    }
    
    public function dieArray($dataArray) {
        echo "<pre>";
        $this->print_r($dataArray);
        die;
    }

    public function print_r($array) {
        if (__DEBUG_MODE__) {
            echo "<pre>";
            if (is_array($array)) {
                print_r($array);
            } else {
                echo strval($array);
            }
            echo "</pre>";
        }
    }
    
}
