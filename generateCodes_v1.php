<?php
error_reporting(E_ERROR);
//require_once 'common.php';
require_once 'constants.php';
require_once 'masterConnection.php';
require_once 'functions.php';
include_once 'globals.php';
require_once DIR_BLL . 'Codes.php';

require_once 'Logger.php';
$logger = new Logger();

$numRolls = isset($_POST[globals::$P_NUM_ROLLS])?$_POST[globals::$P_NUM_ROLLS]:'';
$numLabelsPerRoll = isset($_POST[globals::$P_NUM_LABELS_PER_ROLL])?$_POST[globals::$P_NUM_LABELS_PER_ROLL]:'';
$customerName =  isset($_POST[globals::$P_CUSTOMER_NAME])?$_POST[globals::$P_CUSTOMER_NAME]:'';

$inputs = array("numRolls"=>$numRolls,"numLabelsPerRoll"=>$numLabelsPerRoll,"customerName"=>$customerName);
$result =checkInputs($inputs);

if ($result["error"]) {

	$logger->logEvent('generateCodes', $dataReceived, array('ERROR'=>'F|'.$result["msg"].' field(s) are required.'), 'ERROR_INVALID_INPUT');
	echo 'F|'.$result["msg"].' field(s) are required.';
	die();
}

$totalLabels = $numRolls*$numLabelsPerRoll;
// file name for download
$fileName = $customerName."_scratchcodes_" . date('Ymdhs') . ".csv";
$flag = false;
// headers for download
//header("Content-Disposition: attachment; filename=\"$fileName\"");
//header("Content-Type: application/vnd.ms-excel");
$codeObject = new BLL\Codes($db);
$rollNumber = $codeObject->getLastRollNumber() + 1;
$oid = $codeObject->getLastOID();
//echo "oid".$oid;

$existingRandom = $random = array_flip($codeObject->getAllCodes());
$time_start = microtime(true);
$existingCount = count($random); //The codes you already have

do {
	$random[mt_rand(100000000000,999999999999)] = 1;
} while ((count($random)-$existingCount) < $totalLabels);
$newCodes = array_diff_key($random,$existingRandom);
$newCodes = array_keys($newCodes);

/*$random = array_keys($random);
foreach($random as $value){
	$r[] = $value;
}
*/ 
//$random = array_map('strval', $random);

$aValues = array();
// Don't forget to protect against SQL injection :)
$i=0;

//echo 'a'.(microtime(true) - $time_start)."<br>";


$isActive = 0;
$isValid = 1;
$timestamp = 'now()';
$newCodesArray[] = array('uid','oid');
foreach($newCodes as $row){
	$oid++;
	if($i == $numLabelsPerRoll){$rollNumber++; $i =0;}
	//$aValues[] = '('.(++$oid).','.$row.','.$rollNumber.','."'".$customerName."'".','.$isActive.','.$isValid.','.'now()'.')';
	array_push($aValues,$oid, $row, $rollNumber,$customerName,$isActive,$isValid,$timestamp);
	$newCodesArray[] = array($row,$oid);
	$i++;
}

echo 'b'.(microtime(true) - $time_start)."<br>";
unset($existingRandom);
unset($random);
$codeObject->addCode($aValues,$totalLabels);
convert_to_csv($newCodesArray,$fileName,',');
unset($newCodes);
unset($codeObject);
unset($aValues);
/*
foreach($newCodes as $row){
	$codeObject = new BLL\Codes($db);
	if($i == $numLabelsPerRoll){$rollNumber++;}
	$codeObject->UID = $row;
	$codeObject->OID = ++$oid;
	$codeObject->isActive = 0;
	$codeObject->isValid = 1;
	$codeObject->customerName = $customerName;
	$codeObject->rollNumber = $rollNumber;
	$codeObject->addCode();
	$i++;
}
*/






/*
$random = array();
for ($i = 0; $i < $totalLabels; $i++) {
	do{
	$random[$i] = mt_rand(100000000000,999999999999); //need to optimize this to reduce collisions given the databse will be grow
	//$random[$i]=424742706911;
	}while(isCodeNotUnique($random[$i],$db));
	$codeObject = new BLL\Codes($db);
	$codeObject->UID = $random[$i];
	$codeObject->OID = $oid = $oid+1;
	$codeObject->customerName = $customerName;
	$codeObject->rollNumber = $rollNumber;
	$generatedCodeInfo[$i] = array("UID"=>$random[$i], "OID"=>$oid);//$codeObject->addCode();
	
	//change roll number if totallabesl per roll printed
	if($i == ($numLabelsPerRoll-1)){$rollNumber++;}
	
	/*
	//$generatedCodeInfo[i] = array("UID" => 10001,"OID"=>$random[$i]);
	if(!$flag) {
		// display column names as first row
		echo implode("\t", array_keys($generatedCodeInfo[$i])) . "\n";
		$flag = true;
	}
	// filter data
	array_walk($generatedCodeInfo[$i], 'filterData');
	echo implode("\t", array_values($generatedCodeInfo[$i])) . "\n";
	
	
}
*/
 //echo 'c'.(microtime(true) - $time_start)."<br>";
 



function filterData(&$str)
{
	$str = preg_replace("/\t/", "\\t", $str);
	$str = preg_replace("/\r?\n/", "\\n", $str);
	if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
}

function isCodeNotUnique($random,$db){
	/*
	$codeObject = new BLL\Codes($db);
	$codeObject->UID = $random;
	if(!empty($codeObject->getCodeByUID())){
		return true;
	}
	
	*/
	return false;
}

function convert_to_csv($input_array, $fileName, $delimiter)
{
	
    header('Content-Type: text/csv');
	header("Content-Disposition: attachment; filename=\"$fileName\"");
	/* open raw memory as file, no need for temp files, be careful not to run out of memory thought */
	$f = fopen('php://output', 'w');
	/* loop through array  */
	foreach ($input_array as $line) {
		/* default php csv handler */
		
		fputcsv($f, $line, $delimiter);
	}
	fclose($f) or die("Can't close php://output");

}





