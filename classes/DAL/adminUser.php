<?php

namespace DAL;

use DAL;
use PDO;
require_once DIR_DAL . 'BaseClass.php';
require_once DIR_CLASSES . 'encryption/AES.php';

class AdminUser extends BaseClass {

    public function getUser($userID) {
        $sql = "SELECT * FROM admin_user WHERE id=:userID";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userID', $userID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    
    
    
    public function getUserByEmail($email) {
      
        $sql = "SELECT * FROM admin_user where email = :email";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getUserByCredentials($username, $password) {
        $key = "A2B6762EF2504534C21FC46FB9856648";
        $blockSize = 256;
        $aes = new \AES($password, $key, $blockSize);
        $enc = $aes->encrypt();
        $sql = "SELECT * FROM admin_user WHERE username=:username and password=:password and is_active=1";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username,PDO::PARAM_STR);
        $stmt->bindParam(':password', $enc,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getByUsername($username) {
        $sql = "SELECT * FROM admin_user WHERE username=:username ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username,PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addUser($username, $password,$firstName,$lastName,$email,$storeID,$storeChainID,$roleID,$isActive,$isAdmin) {
        $customerID = time() . mt_rand(1000,9999);
        $sql = "INSERT INTO admin_user(username,password,first_name,last_name,email,store_id,store_chain_id,role_id,is_active,is_admin,created_on) VALUES(:username,:password,:firstName,:lastName,:email,:storeID,:storeChainID,:roleID,:isActive,:isAdmin,now())";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':firstName', $firstName);
        $stmt->bindParam(':lastName', $lastName);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':storeID', $storeID);
        $stmt->bindParam(':storeChainID', $storeChainID);
        $stmt->bindParam(':roleID', $roleID);
        $stmt->bindParam(':isActive', $isActive);
        $stmt->bindParam(':isAdmin', $isAdmin);
        $result = $stmt->execute();
        if ($result == false)
            return 0;
        else
            return array($this->db->lastInsertId(),$customerID);
    }
    
    public function addPermission($permissionArray,$userID){
       
        if(is_array($permissionArray)){
            foreach($permissionArray as $permission){
                
                 $sql = "INSERT INTO user_dashboardaccess_permission(userid,dp_id) VALUES($userID,$permission)";
                 $stmt = $this->db->prepare($sql);
                $result = $stmt->execute();
               
            }
             return $result;
        }
        
    }
    
    public function updatePermission($userID, $permissionArray)
    {
        $sql = "UPDATE user_dashboardaccess_permission SET deleted = 1 WHERE userid = :userid ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userid', $userID);
        $stmt->execute();
        
        return $this->addPermission($permissionArray, $userID);
                
    }
    
    
    public function getDashboardPermission($userID){
        
        $sql = "SELECT dp_id as dp FROM user_dashboardaccess_permission WHERE userid = :userid AND deleted = 0";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userid', $userID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
        
    }

    public function resetPasswordLink($userID, $matchingString)
    {
        $sql = "INSERT INTO admin_users_password_reset(userid_fk,matching_string,created_datetime) VALUES(:userid, :matchingstring, now())";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userid', $userID);
        $stmt->bindParam(':matchingstring', $matchingString);
        $result = $stmt->execute();
        if ($result == false)
            return 0;
        else
            return $this->db->lastInsertId();        
    }
    
    public function resetAdminUsersPassword($userID, $password, $hashID)
    {
        $sql = "UPDATE admin_user SET password = '$password' WHERE id = $userID";
		
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        
        $sql = "UPDATE admin_users_password_reset SET deleted = 1 WHERE id = $hashID";
		$stmt = $this->db->prepare($sql);
        return $stmt->execute();
    }
    
    
    public function getAdminHashedLink($userID)
    {
        $sql = "SELECT * FROM admin_users_password_reset WHERE userid_fk = :userid AND deleted = 0 ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam(':userid', $userID);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    
    
    public function getHashedDetails($hash)
    {
      
        $sql = "SELECT * FROM admin_users_password_reset WHERE matching_string = '".$hash."' AND deleted = 0 ";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    
    public function deleteHashedLink($id)
    {
        $sql = "UPDATE admin_users_password_reset SET deleted = 1 WHERE id = $id";
		$stmt = $this->db->prepare($sql);
        return $stmt->execute();
    }
    
    public function getAllAdminUsers(){
        if($_SESSION['roleID']==1){
                $sql= "SELECT * FROM admin_user au LEFT JOIN web_user_role wur ON au.role_id=wur.roleid";
         }elseif($_SESSION['roleID']==3){
            
              $storeChainID = $_SESSION['storeChainID'];
           
$sql= "SELECT * FROM admin_user au LEFT JOIN web_user_role wur ON wur.roleid=au.role_id WHERE au.store_chain_id=$storeChainID and au.role_id=4";
           
        }
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }
    
    public function updateUserByID($userID,$firstName,$lastName,$email){
        $sql="UPDATE admin_user SET first_name=:firstname,last_name=:lastname,email=:email WHERE id=:userid";
        $stmt = $this->db->prepare($sql);
         $stmt->bindParam(':userid', $userID);
         $stmt->bindParam(':firstname', $firstName);
         $stmt->bindParam(':lastname', $lastName);
         $stmt->bindParam(':email', $email);
         return $stmt->execute();
        
    }
    
    public function deleteUserByID($userID){
        $sql="DELETE FROM admin_user WHERE id=:uid";
        $stmt = $this->db->prepare($sql);
         $stmt->bindParam(':uid', $userID);
         return $stmt->execute();
    }
    
    
}
