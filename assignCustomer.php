<?php
error_reporting(E_ERROR);
//require_once 'common.php';
require_once 'constants.php';
require_once 'masterConnection.php';
require_once 'functions.php';
include_once 'globals.php';
require_once DIR_BLL . 'Codes.php';

require_once 'Logger.php';
$logger = new Logger();

$rollNumber = isset($_POST[globals::$P_ROLL_NUMBER])?$_POST[globals::$P_ROLL_NUMBER]:'';
$customerName = isset($_POST[globals::$P_CUSTOMER_NAME])?$_POST[globals::$P_CUSTOMER_NAME]:'';


$inputs = array("rollNumber"=>$rollNumber, "customerName"=>$customerName);
$result =checkInputs($inputs);

if ($result["error"]) {

	$logger->logEvent('generateCodes', $dataReceived, array('ERROR'=>'F|'.$result["msg"].' field(s) are required.'), 'ERROR_INVALID_INPUT');
	echo 'F|'.$result["msg"].' field(s) are required.';
	die();
}

$codeObject = new BLL\Codes($db);
$codeObject->rollNumber = $rollNumber;
$codeObject->customerName = $customerName;

$result = $codeObject->assignCustomerByRollNumber();
if($result == true){
	echo"Codes for Roll Number ".$rollNumber." were assigned to customer ".$customerName;
}








